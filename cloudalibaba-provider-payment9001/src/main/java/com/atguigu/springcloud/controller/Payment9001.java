package com.atguigu.springcloud.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Payment9001 {

    @Value("${server.port}")
    private String sortPort;

    @GetMapping(value="/payment/nacos/{id}")
    public String getPayment(@PathVariable Integer id){
        return "nacos registry sortPort: "+sortPort+"\t"+id;
    }
}
