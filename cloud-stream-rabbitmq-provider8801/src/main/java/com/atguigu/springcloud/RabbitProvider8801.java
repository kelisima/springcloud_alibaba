package com.atguigu.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RabbitProvider8801 {

    public static void main(String[] args) {
        SpringApplication.run(RabbitProvider8801.class,args);
    }
}
