package com.atguigu.springcloud.entities;

public class CommonResult<T> {

    public static final Integer fail=444;
    public static final Integer success=200;

    private Integer code;

    private T data;

    private String message;

    public CommonResult() {
    }

    public CommonResult(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public CommonResult(Integer code,  String message,T data) {
        this.code = code;
        this.data = data;
        this.message = message;
    }

    public  void fail(String message){
        this.message=message;
        this.code=fail;
    }

    public void success(T data){
        this.data=data;
        this.code=success;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
