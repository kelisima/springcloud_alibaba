package com.atguigu.springcloud.util;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("分页查询")
public class PageQuery {

    @ApiModelProperty("每页几条")
    private int size=10;            //当前到第几条

    @ApiModelProperty("当前页号")
    private int current=0;           //当前页号

    @ApiModelProperty("排序")
    private String sortBy;

    @ApiModelProperty("模块")
    private String modular;

    public Page getMPage(){
        Page page=new Page();
        page.setCurrent(this.current);
        page.setSize(this.size);
        return page;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getCurrent() {
        return current;
    }

    public void setCurrent(int current) {
        this.current = current;
    }

    public String getSortBy() {
        return sortBy;
    }

    public void setSortBy(String sortBy) {
        this.sortBy = sortBy;
    }

    public String getModular() {
        return modular;
    }

    public void setModular(String modular) {
        this.modular = modular;
    }
}
