package com.atguigu.springcloud.config;

import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @Author:Ru.wang Description:
 * Date:Created in 15:05 2018/5/4
 * Modified By：
 */
@Configuration
@EnableSwagger2
public class Swagger {

    public Docket createRestApi(){

        return  new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.angenin.springcloud.controller"))
                .paths(PathSelectors.any())
//                .paths(Predicates.not(PathSelectors.regex("/BasicErrorController.*")))// 错误路径不监控
//                .paths(PathSelectors.regex("/.*"))// 对根下所有路径进行监控
                .build();
    }
    private ApiInfo apiInfo(){

        return new ApiInfoBuilder()
                .title("Spring boot中使用Swagger2构建RESTful APIs")
                .description("swagger")
                .termsOfServiceUrl("")
                .version("")
                .build();
    }




}
