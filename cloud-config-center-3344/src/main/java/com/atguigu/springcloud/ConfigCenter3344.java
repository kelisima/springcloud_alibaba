package com.atguigu.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.config.server.EnableConfigServer;

@SpringBootApplication
@EnableConfigServer
public class ConfigCenter3344 {
//    全局刷新配置文件
//    curl -X POST "http://localhost:3344/actuator/bus-refresh"
    //部分刷新配置文件
//    curl -X POST "http://localhost:3344/actuator/bus-refresh/config-client:3355"
    public static void main(String[] args) {
        SpringApplication.run(ConfigCenter3344.class,args);
    }
}
