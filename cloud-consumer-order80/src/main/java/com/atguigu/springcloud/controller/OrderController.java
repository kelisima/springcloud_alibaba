package com.atguigu.springcloud.controller;

import com.atguigu.springcloud.entities.CommonResult;
import com.atguigu.springcloud.entities.Payment;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;

@RestController
@RequestMapping()
@Slf4j
public class OrderController {

    @Value("${restApi.payment.create}")
    private String paymentCreate;
    @Value("${restApi.payment.getById}")
    private String paymentGetById;
    @Resource
    private RestTemplate restTemplate;

    @PostMapping("consumer/payment")
    public CommonResult create(Payment payment){

        return restTemplate.postForObject(paymentCreate,payment,CommonResult.class);
    }

    @GetMapping("consumer/payment/get/{id}")
    public CommonResult getPaymentById(@PathVariable Long id){
        return restTemplate.getForObject(paymentGetById+String.valueOf(id),CommonResult.class);
    }

    @GetMapping("consumer/payment/zipkin")
    public String zipkin(){
        return restTemplate.getForObject("http://cloud-payment-service"+"/payment/zipkin",String.class);
    }
}
