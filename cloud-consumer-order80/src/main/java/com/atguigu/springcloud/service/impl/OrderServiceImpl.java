//package com.angenin.springcloud.service.impl;
//
//import com.angenin.springcloud.dao.OrderMapper;
//import com.angenin.springcloud.entities.Payment;
//import com.angenin.springcloud.service.OrderService;
//import com.angenin.springcloud.util.PageQuery;
//import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
//import com.baomidou.mybatisplus.core.metadata.IPage;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//
//import java.util.List;
//
//@Service
//public class OrderServiceImpl implements OrderService {
//
//    @Autowired
//    private OrderMapper mapper;
//
//    @Override
//    public void create(Payment payment) {
//        mapper.insert(payment);
//    }
//
//    @Override
//    public void updateById(Payment payment) {
//        mapper.updateById(payment);
//    }
//
//    @Override
//    public void delete(Long id) {
//        mapper.deleteById(id);
//    }
//
//    @Override
//    public List<Payment> findSelf(Payment payment, PageQuery pageQuery) {
//        return mapper.selectList(queryWrapper(payment,pageQuery));
//    }
//
//    @Override
//    public IPage<Payment> findPage(Payment payment, PageQuery pageQuery) {
//        return mapper.selectPage(pageQuery.getMPage(),queryWrapper(payment,pageQuery));
//    }
//
//
//    @Override
//    public Payment getPaymentById(Long id) {
//
//        Payment payment=new Payment();
//        payment.setId(id);
//        return mapper.selectOne(queryWrapper(payment,new PageQuery()));
//    }
//
//    private QueryWrapper queryWrapper(Payment payment, PageQuery pageQuery){
//        QueryWrapper wrapper=new QueryWrapper();
//
//        if(payment.getId()!=null) {
//            wrapper.eq("id", payment.getId());
//        }
//        if(payment.getSerial()!=null&&!"".equals(payment.getSerial())) {
//            wrapper.eq("serial", payment.getSerial());
//        }
//        if(pageQuery.getSortBy()!=null&&!"".equals(pageQuery.getSortBy())){
//            wrapper.orderByDesc(pageQuery.getSortBy());
//        }
//        return wrapper;
//    }
//}
