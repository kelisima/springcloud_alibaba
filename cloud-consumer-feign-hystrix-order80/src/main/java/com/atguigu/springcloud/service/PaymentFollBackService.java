package com.atguigu.springcloud.service;

import org.springframework.stereotype.Component;

@Component
public class PaymentFollBackService implements PaymentFeignService {
    @Override
    public String ok(Long id) {
        return "PaymentFollBackService foll back ok";
    }

    @Override
    public String timeout(Long id) {
        return "PaymentFollBackService foll back timeout";
    }
}
