package com.atguigu.springcloud.controller;

import com.atguigu.springcloud.entities.CommonResult;
import com.atguigu.springcloud.service.PaymentFeignService;
import com.netflix.hystrix.contrib.javanica.annotation.DefaultProperties;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.UUID;

@RestController
//@DefaultProperties(defaultFallback = "payment_Global_FallbackMethod")
public class PaymentFeignController {

    @Resource
    private PaymentFeignService service;

//    @GetMapping("consumer/payment/get/{id}")
//    public CommonResult getPaymentById(@PathVariable Long id){
//        return service.getPaymentById(id);
//    }
//
//    @GetMapping("consumer/payment/feign/timeout")
//    public String paymentFeignTimeout(){
//        return service.paymentFeignTimeout();
//    }

    @GetMapping("consumer/payment/hystrix/ok/{id}")
    public String getPaymentById(@PathVariable Long id){
        return service.ok(id);
    }

    @GetMapping("consumer/payment/hystrix/timeout/{id}")
//    @HystrixCommand
//            (fallbackMethod = "paymentInfo_timeout_handler",commandProperties = {
//            @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds",value="1500")
//    })
    public String paymentFeignTimeout(@PathVariable Long id){
        return service.timeout(id);
    }

    public String paymentInfo_timeout_handler(@PathVariable Long id){
        return "线程池: "+Thread.currentThread().getName()+" 系统繁忙或者运行报错,请稍后再试: "+" 哈哈";
    }

    /**
     * 全局follback
     */
    public String payment_Global_FallbackMethod(){
        return "global异常处理信息,请稍后再试";
    }


}
