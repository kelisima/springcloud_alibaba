package com.atguigu.springcloud.controller;

import com.atguigu.springcloud.entities.CommonResult;
import com.atguigu.springcloud.service.PaymentService;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;


@RestController
public class PaymentController {

    @Value("${server.port}")
    private String serverPort;
    @Autowired
    private PaymentService paymentService;

    @GetMapping("payment/hystrix/ok/{id}")
    public CommonResult ok(@PathVariable Integer id){
        return new CommonResult(200,paymentService.paymentInfo_OK(id));
    }

    @GetMapping("payment/hystrix/timeout/{id}")
    public CommonResult timeout(@PathVariable Integer id){
        return new CommonResult(400,paymentService.paymentInfo_timeout(id));
    }

    @GetMapping("payment/hystrix/circuit/{id}")
    @HystrixCommand(fallbackMethod="paymentCircuitBreaker_fallback",commandProperties={
            @HystrixProperty(name="circuitBreaker.enabled",value="true"),
            @HystrixProperty(name="circuitBreaker.requestVolumeThreshold",value="10"),
            @HystrixProperty(name="circuitBreaker.sleepWindowInMilliseconds",value="10000"),
            @HystrixProperty(name="circuitBreaker.errorThresholdPercentage",value="60"),
    })
    public String paymentCircuitBreaker(@PathVariable Integer id){
        if(id <0){
            throw new RuntimeException("id不能负数");
        }
        String serl= UUID.randomUUID().toString().replace("-","");
        return Thread.currentThread().getName()+"\t"+"调用成功,流水号: "+serl;
    }

    @GetMapping("payment/lb")
    public String getLB(){
        return serverPort;
    }

    public String paymentCircuitBreaker_fallback(@PathVariable Integer id){
        return "id不能负数,paymentCircuitBreaker_fallback";
    }

    @GetMapping("payment/zipkin")
    public String zipkin(){
        return " hi, im payment zipkin fallback,welcome to atguigu";
    }
}
