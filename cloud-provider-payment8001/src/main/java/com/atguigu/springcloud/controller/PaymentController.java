package com.atguigu.springcloud.controller;

import com.atguigu.springcloud.entities.CommonResult;
import com.atguigu.springcloud.entities.Payment;
import com.atguigu.springcloud.service.PaymentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.concurrent.TimeUnit;


@RestController
@RequestMapping()
public class PaymentController {

    @Autowired
    private PaymentService paymentService;
    @Value("${server.port}")
    private String serverPort;
    @Resource
    private DiscoveryClient discoveryClient;
    private Logger logger= LoggerFactory.getLogger(this.getClass());

    @PostMapping("payment")
    public CommonResult create(Payment payment){

        try {
            paymentService.create(payment);
            return new CommonResult(200,"插入数据库成功,serverPort:"+serverPort);
        } catch (Exception e) {
            return new CommonResult(444,"插入数据库失败,serverPort:"+serverPort+e.getLocalizedMessage());
        }
    }

    @GetMapping("payment/id/{id}")
    public CommonResult create(@PathVariable Long id){

        try {
            Payment object = paymentService.getPaymentById(id);
            return new CommonResult(200,"查询数据库成功,serverPort:"+serverPort,object);
        } catch (Exception e) {
            e.printStackTrace();
            return new CommonResult(444,"查询数据库失败,serverPort:"+serverPort+e.getLocalizedMessage());
        }
    }

    @GetMapping("payment/discovery")
    public Object discovery(){
        List<String> services = discoveryClient.getServices();
        for (String service : services) {
            logger.info("element:{}",service);
        }
        List<ServiceInstance> instances = discoveryClient.getInstances("CLOUD-PAYMENT-SERVICE");
        for (ServiceInstance instance : instances) {
            logger.info(instance.getServiceId()+"\t"+instance.getHost()+"\t"+instance.getPort()+"\t"+instance.getUri());
        }
        return this.discoveryClient;
    }

    @GetMapping("payment/feign/timeout")
    public String paymentFeignTimeout(){
        try {
            TimeUnit.SECONDS.sleep(3);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return serverPort;
    }

    @GetMapping("payment/lb")
    public String getLB(){
        return serverPort;
    }

    @GetMapping("payment/zipkin")
    public String zipkin(){
        return " hi, im payment zipkin fallback,welcome to atguigu";
    }
}
