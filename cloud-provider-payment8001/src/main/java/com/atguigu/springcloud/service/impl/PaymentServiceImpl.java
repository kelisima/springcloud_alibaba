package com.atguigu.springcloud.service.impl;

import com.atguigu.springcloud.dao.PaymentMapper;
import com.atguigu.springcloud.entities.Payment;
import com.atguigu.springcloud.service.PaymentService;
import com.atguigu.springcloud.util.PageQuery;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PaymentServiceImpl implements PaymentService {

    @Autowired
    private PaymentMapper mapper;

    @Override
    public void create(Payment payment) {
        mapper.insert(payment);
    }

    @Override
    public void updateById(Payment payment) {
        mapper.updateById(payment);
    }

    @Override
    public void delete(Long id) {
        mapper.deleteById(id);
    }

    @Override
    public List<Payment> findSelf(Payment payment, PageQuery pageQuery) {
        return mapper.selectList(queryWrapper(payment,pageQuery));
    }

    @Override
    public IPage<Payment> findPage(Payment payment, PageQuery pageQuery) {
        return mapper.selectPage(pageQuery.getMPage(),queryWrapper(payment,pageQuery));
    }


    @Override
    public Payment getPaymentById(Long id) {

        Payment payment=new Payment();
        payment.setId(id);
        return mapper.selectOne(queryWrapper(payment,new PageQuery()));
    }

    private QueryWrapper queryWrapper(Payment payment, PageQuery pageQuery){
        QueryWrapper wrapper=new QueryWrapper();

        if(payment.getId()!=null) {
            wrapper.eq("id", payment.getId());
        }
        if(payment.getSerial()!=null&&!"".equals(payment.getSerial())) {
            wrapper.eq("serial", payment.getSerial());
        }
        if(pageQuery.getSortBy()!=null&&!"".equals(pageQuery.getSortBy())){
            wrapper.orderByDesc(pageQuery.getSortBy());
        }
        return wrapper;
    }
}
