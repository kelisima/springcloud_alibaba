package com.atguigu.springcloud.service;

import com.atguigu.springcloud.entities.Payment;
import com.atguigu.springcloud.util.PageQuery;
import com.baomidou.mybatisplus.core.metadata.IPage;

import java.util.List;

public interface PaymentService {

    void create(Payment payment);
    void updateById(Payment payment);
    void delete(Long id);
    List<Payment> findSelf(Payment payment, PageQuery pageQuery);
    IPage<Payment> findPage(Payment payment, PageQuery pageQuery);

    Payment getPaymentById(Long id);
}
